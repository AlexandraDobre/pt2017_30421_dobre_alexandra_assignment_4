package processing;

import model.Account;
import model.Person;

public interface BankProc {

	boolean addPersons(Person person);

	boolean removePersons(Person person);

	boolean addAccount(Account account, Person person);

	boolean readDataAccounts();

	void writeAccountsData();

	boolean removeAccount(Account account, Person person);
}
