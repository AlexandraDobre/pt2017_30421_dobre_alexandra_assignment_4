package junit;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

import model.Person;
import model.SavingAccount;
import model.SpendingAccount;
import processing.Bank;

public class JUnit {

	@Test
	public void testWithdraw() {
		Person person = new Person("Andreas", 1, 20, "Cluj");
		Date date = new Date();
		SpendingAccount account = new SpendingAccount(person, 20f, 1, 3);
		Bank.getInstance().addPersons(person);
		Bank.getInstance().addAccount(account, person);
		Bank.getInstance().withdrawMoneySpendingAccount(account, person, 10f);
		System.out.println((int)account.getAvailableSum());
		assertEquals((int)10f, (int)account.getAvailableSum());
		
		
	//	withdrawMoneySpendingAccount();
	}
	
	@Test
	public void testAddMoney() {
		Person person1 = new Person("Ale", 2, 21, "Cluj-napoca");
		Date date = new Date();
		SpendingAccount account1 = new SpendingAccount(person1, 10f, 2, 4);
		Bank.getInstance().addPersons(person1);
		Bank.getInstance().addAccount(account1, person1);
		Bank.getInstance().addMoneySpendingAccount(account1, person1, 30f);
		System.out.println((int)account1.getAvailableSum());
		assertNotEquals((int)40f, (int)account1.getAvailableSum());
		
	//	withdrawMoneySpendingAccount();
	}
	
	
	@Test
	public void testAddPerson(){
		Person person2 = new Person("Margin", 3, 23, "Zalau");
		int size = Bank.getInstance().findPersons().size();
		Bank.getInstance().addPersons(person2);
		assertEquals(size, Bank.getInstance().findPersons().size());
		
	}
}
