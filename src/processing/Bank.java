package processing;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.Account;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;

public class Bank implements BankProc, Serializable {

	private static final long serialVersionUID = 2781530330291522264L;

	private transient static Bank INSTANCE = null;

	public static final String DATA_FILE = "data.dat";

	public static Bank getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Bank();
			INSTANCE.readDataAccounts();
		}
		return INSTANCE;
	}

	private Map<Person, List<Account>> bankEntries;

	public Bank() {
		bankEntries = new HashMap<>();
	}

	@Override
	public boolean addPersons(Person person) {
		//assert !bankEntries.containsKey(person) : "Person already exists!";
		boolean wellFormed = isWellFormed();
		assert !wellFormed : "Bank not well formed!";
		assert person.getId()!=0 :"Person ID should not be 0";
		int age = person.getAge();
		bankEntries.put(person, new ArrayList<>());
		assert(age == person.getAge());
		return true;
	}

	@Override
	public boolean removePersons(Person person) {
	//assert !bankEntries.containsKey(person) : "Person already deleted!";
		boolean wellFormed = isWellFormed();
		assert !wellFormed : "Bank not well formed!";
		if (bankEntries.containsKey(person)) {
			bankEntries.remove(person, new ArrayList<>());
			return true;
		}
		return false;
	}
	
	public void removePersonById(int id){
		boolean wellFormed = isWellFormed();
		assert !wellFormed : "Bank not well formed!";
		ArrayList<Person> persons = getPersons();
		for(Person person:persons){
			if(person.getId() == id){
				int size = bankEntries.keySet().size();
				bankEntries.remove(person);
				assert(size-1 == bankEntries.keySet().size());
			}
		}
	}
	
	public boolean updatePerson(Person person) {
		boolean wellFormed = isWellFormed();
		assert !wellFormed : "Bank not well formed!";
		assert bankEntries.containsKey(person) : "This person is not a client of this bank!";
		if(bankEntries.containsKey(person)){
			person.setAddress(person.getAddress());
			person.setAge(person.getAge());
			person.setName(person.getName());
			return true;
		}
		return false;
	}
	
	@Override
	public boolean addAccount(Account account, Person person) {
		boolean wellFormed = isWellFormed();
		assert !wellFormed : "Bank not well formed!";
		//assert bankEntries.containsKey(person) : "Person not in the system!";
		List<Account> accounts = bankEntries.get(person);
		if (accounts.contains(account)) {
			return false;
		}
		accounts.add(account);
		return true;
	}

	@Override
	public boolean removeAccount(Account account, Person person) {
		boolean wellFormed = isWellFormed();
		assert !wellFormed : "Bank not well formed!";
		List<Account> accounts = bankEntries.get(person);
		if (accounts == null || !accounts.contains(account)) {
			return false;
		}
		accounts.remove(account);
		return true;
	}

	@Override
	public boolean readDataAccounts() {
		try (ObjectInputStream stream = new ObjectInputStream(new FileInputStream(new File(DATA_FILE)))) {
			INSTANCE = (Bank) stream.readObject();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public void writeAccountsData() {
		try (ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(new File(DATA_FILE)))) {
			stream.writeObject(this);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public boolean addMoneySavingAccount(SavingAccount account, Person person, float sumToBeAdded) {
		boolean wellFormed = isWellFormed();
		assert !wellFormed : "Bank not well formed!";
		if (bankEntries.containsKey(person)) {
			List<Account> accounts = bankEntries.get(person);
			if (accounts == null || !accounts.contains(account)) {
				return false;
			}
			for (Account accountPerson : accounts) {
				if (accountPerson.equals(account)) {
					float currentSum = accountPerson.getAvailableSum();
					currentSum = currentSum + sumToBeAdded;
					accountPerson.setAvailableSum(currentSum);
					//accountPerson.addObserver(person);
					return true;
				}
			}
			return true;
		}
		return false;
	}

	public boolean withdrawMoneySavingAccount(SavingAccount account, Person person, float sumToBeWithdrawn) {
		boolean wellFormed = isWellFormed();
		assert !wellFormed : "Bank not well formed!";
		if (bankEntries.containsKey(person)) {
			List<Account> accounts = bankEntries.get(person);
			if (accounts == null || !accounts.contains(account)) {
				return false;
			}
			for (Account accountPerson : accounts) {
				if (accountPerson.equals(account)) {
					float currentSum = accountPerson.getAvailableSum();
					if ((currentSum >= sumToBeWithdrawn) && (account.getMinSum() <= currentSum - sumToBeWithdrawn)) {
						currentSum = currentSum - sumToBeWithdrawn;
						accountPerson.setAvailableSum(currentSum);
						//accountPerson.addObserver(person);
						return true;
					}

				}
			}
		}
		return false;
	}

	public boolean addMoneySpendingAccount(SpendingAccount account, Person person, float sumToBeAdded) {
		boolean wellFormed = isWellFormed();
		assert !wellFormed : "Bank not well formed!";
		if (bankEntries.containsKey(person)) {
			List<Account> accounts = bankEntries.get(person);
			if (accounts == null || !accounts.contains(account)) {
				return false;
			}
			for (Account accountPerson : accounts) {
				if (accountPerson.equals(account)) {
					float currentSum = accountPerson.getAvailableSum();
					currentSum = currentSum + sumToBeAdded + sumToBeAdded*account.getCurrentInterest()/100;
					accountPerson.setAvailableSum(currentSum);
					//accountPerson.addObserver(person);
					return true;
				}
			}
			return true;
		}
		return false;
	}

	public boolean withdrawMoneySpendingAccount(SpendingAccount account, Person person, float sumToBeWithdrawn) {
		boolean wellFormed = isWellFormed();
		assert !wellFormed : "Bank not well formed!";
		if (bankEntries.containsKey(person)) {
			List<Account> accounts = bankEntries.get(person);
			if (accounts == null || !accounts.contains(account)) {
				return false;
			}
			for (Account accountPerson : accounts) {
				if (accountPerson.equals(account)) {
					float currentSum = accountPerson.getAvailableSum();
					if (currentSum >= sumToBeWithdrawn) {
						currentSum = currentSum - sumToBeWithdrawn;
						accountPerson.setAvailableSum(currentSum);
						//accountPerson.addObserver(person);
						return true;
					}

				}
			}
		}
		return false;
	}

	public ArrayList<Person> getPersons(){
		
		ArrayList<Person> personList = new ArrayList<Person>();
		for(Person person : bankEntries.keySet()){
			personList.add(person);
		}
		return personList;
	}
	
	public Map<Person, List<Account>> getBankEntries() {
		return bankEntries;
	}

	public Collection<Person> findPersons() {
		return bankEntries.keySet();
	}

	public List<SpendingAccount> findSpendingAccount() {
		List<SpendingAccount> result = new ArrayList<>();
		for (List<Account> accountList : bankEntries.values()) {
			if (accountList == null) {
				continue;
			}
			for (Account account : accountList) {
				if (account instanceof SpendingAccount) {
					result.add((SpendingAccount) account);
				}
			}
		}
		return result;
	}

	public List<SavingAccount> findSavingAccount() {
		List<SavingAccount> result = new ArrayList<>();
		for (List<Account> accountList : bankEntries.values()) {
			if (accountList == null) {
				continue;
			}
			for (Account account : accountList) {
				if (account instanceof SavingAccount) {
					result.add((SavingAccount) account);
				}
			}
		}
		return result;
	}

	public boolean isWellFormed(){
		if(bankEntries.isEmpty() == false){
			return false;
		}				
		return true;
		
	}
}
