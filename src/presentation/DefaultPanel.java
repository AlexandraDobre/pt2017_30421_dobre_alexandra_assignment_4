package presentation;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class DefaultPanel extends JPanel {
	private static final long serialVersionUID = -707456712169721259L;

	public DefaultPanel(String title) {
		setLayout(null);
		setBackground(Color.DARK_GRAY);
		JLabel label = new JLabel(title);
		label.setBounds(350, 50, 500, 50);
		label.setFont(new Font("TimesRoman", Font.PLAIN, 30));
		label.setForeground(Color.pink);
		add(label);
	}

	public void addInputLabel(String text, int x, int y, int width, int height) {
		JLabel label = new JLabel(text);
		label.setBounds(x, y, width, height);
		label.setFont(new Font("TimesRoman", Font.PLAIN, 20));
		label.setForeground(Color.white);
		add(label);
	}
}
