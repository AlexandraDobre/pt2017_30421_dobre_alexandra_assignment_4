package presentation.spendingAccount;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import model.SpendingAccount;
import presentation.DefaultPanel;
import processing.Bank;

public class UpdateSpendingAccountPanel extends DefaultPanel {

	private static final long serialVersionUID = -4375942800712256409L;

	private final JTextField money;
	private final JComboBox<SpendingAccount> spendingAccount;

	public UpdateSpendingAccountPanel() {
		super("Update spending account");
		money = new JTextField();
		List<SpendingAccount> spendingAccountList = (List<SpendingAccount>) Bank.getInstance().findSpendingAccount();
		spendingAccount = new JComboBox<SpendingAccount>(new Vector<>(spendingAccountList));
		createInputFields();
	}

	public void createInputFields() {

		addInputLabel("Spending Account:", 30, 100, 150, 40);
		spendingAccount.setBounds(200, 100, 300, 30);
		add(spendingAccount);

		addInputLabel("Money:", 30, 150, 150, 40);
		money.setBounds(200, 150, 300, 30);
		add(money);

		JButton addButton = new JButton("Add money");
		addButton.setBounds(450, 350, 100, 50);
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String moneyTextValue = money.getText();
				Float moneyValue;
				try {
					moneyValue = Float.valueOf(moneyTextValue);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, "Money is not a number!");
					return;
				}
				if (!(spendingAccount.getSelectedItem() instanceof SpendingAccount)) {
					JOptionPane.showMessageDialog(null, "Select first account!");
					return;
				}
				
				SpendingAccount account = (SpendingAccount) spendingAccount.getSelectedItem();
				if (Bank.getInstance().addMoneySpendingAccount(account, account.getPerson(), moneyValue)) {
					JOptionPane.showMessageDialog(null, "Money add!");
				} else {
					JOptionPane.showMessageDialog(null, "Unable to add money!");
				}
			}
		});
		add(addButton);
		
		JButton withdrawButton = new JButton("Withdraw money");
		withdrawButton.setBounds(450, 450, 100, 50);
		withdrawButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String moneyTextValue = money.getText();
				Float moneyValue;
				try {
					moneyValue = Float.valueOf(moneyTextValue);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, "Money is not a number!");
					return;
				}
				if (!(spendingAccount.getSelectedItem() instanceof SpendingAccount)) {
					JOptionPane.showMessageDialog(null, "Select first account!");
					return;
				}
				
				SpendingAccount account = (SpendingAccount) spendingAccount.getSelectedItem();
				if (Bank.getInstance().withdrawMoneySpendingAccount(account, account.getPerson(), moneyValue)) {
					JOptionPane.showMessageDialog(null, "Money withdrawn!");
				} else {
					JOptionPane.showMessageDialog(null, "Unable to withdraw money!");
				}
			}
		});
		
		add(withdrawButton);

	}
}
