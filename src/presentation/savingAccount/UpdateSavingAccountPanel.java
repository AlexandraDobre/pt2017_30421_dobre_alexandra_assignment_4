package presentation.savingAccount;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import model.SavingAccount;
import presentation.DefaultPanel;
import processing.Bank;

public class UpdateSavingAccountPanel extends DefaultPanel {

	private static final long serialVersionUID = -4375942800712256409L;

	private final JTextField money;
	private final JComboBox<SavingAccount> savingAccount;

	public UpdateSavingAccountPanel() {
		super("Update saving account");
		money = new JTextField();
		List<SavingAccount> savingAccountList = Bank.getInstance().findSavingAccount();
		savingAccount = new JComboBox<SavingAccount>(new Vector<>(savingAccountList));
		createInputFields();
	}

	public void createInputFields() {

		addInputLabel("Saving Account:", 30, 100, 150, 40);
		savingAccount.setBounds(200, 100, 300, 30);
		add(savingAccount);

		addInputLabel("Money:", 30, 150, 150, 40);
		money.setBounds(200, 150, 300, 30);
		add(money);

		JButton addButton = new JButton("Add money");
		addButton.setBounds(450, 350, 100, 50);
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String moneyTextValue = money.getText();
				Float moneyValue;
				try {
					moneyValue = Float.valueOf(moneyTextValue);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, "Money is not a number!");
					return;
				}
				if (!(savingAccount.getSelectedItem() instanceof SavingAccount)) {
					JOptionPane.showMessageDialog(null, "Select first account!");
					return;
				}
				
				SavingAccount account = (SavingAccount) savingAccount.getSelectedItem();
				if (Bank.getInstance().addMoneySavingAccount(account, account.getPerson(), moneyValue)) {
					JOptionPane.showMessageDialog(null, "Money add!");
				} else {
					JOptionPane.showMessageDialog(null, "Unable to add money!");
				}
			}
		});
		add(addButton);
		
		JButton withdrawButton = new JButton("Withdraw money");
		withdrawButton.setBounds(450, 450, 100, 50);
		withdrawButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String moneyTextValue = money.getText();
				Float moneyValue;
				try {
					moneyValue = Float.valueOf(moneyTextValue);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, "Money is not a number!");
					return;
				}
				if (!(savingAccount.getSelectedItem() instanceof SavingAccount)) {
					JOptionPane.showMessageDialog(null, "Select first account!");
					return;
				}
				
				SavingAccount account = (SavingAccount) savingAccount.getSelectedItem();
				if (Bank.getInstance().withdrawMoneySavingAccount(account, account.getPerson(), moneyValue)) {
					JOptionPane.showMessageDialog(null, "Money withdrawn!");
				} else {
					JOptionPane.showMessageDialog(null, "Unable to withdraw money!");
				}
			}
		});
		
		add(withdrawButton);

	}
}
