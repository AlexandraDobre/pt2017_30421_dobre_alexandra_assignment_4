package presentation;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import model.Person;
import processing.Bank;

public abstract class ViewObjectsPanel<T> extends DefaultPanel {
	private final Class<T> type;
	private static final long serialVersionUID = -2611925326465735009L;

	@SuppressWarnings("unchecked")
	public ViewObjectsPanel(String pageTitle) {
		super(pageTitle);
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		initializeData();
	}

	public void initializeData() {
		String[] columns = getColumns();
		Object[][] data = getData(columns);
		for (int i = 0; i < columns.length; i++) {
			columns[i] = columns[i].substring(0, 1).toUpperCase() + columns[i].substring(1).toLowerCase();
		}
		JTable table = new JTable(data, columns);		
		JScrollPane pane = new JScrollPane(table);
		pane.setBounds(50, 100, 900, 500);
		add(pane);
		table.addMouseListener(new java.awt.event.MouseAdapter(){
			public void mouseClicked(java.awt.event.MouseEvent e)
			{
			int row = table.rowAtPoint(e.getPoint());
			int col = table.columnAtPoint(e.getPoint());
			//int row1=table.rowAtPoint(e.getPoint());
			//int col1= table.columnAtPoint(e.getPoint());
			JOptionPane.showMessageDialog(null," Person with the first name :"+ " " +table.getValueAt(row,col).toString() + "was deleted from the bank");
			ArrayList <Person> persons = Bank.getInstance().getPersons();
			for(Person person: persons){
				if(person.getName() == table.getValueAt(row, col).toString())
					Bank.getInstance().removePersonById(person.getId());
					getData(columns);
			}
			
			}
			
			}
			);
		}

	

	public String[] getColumns() {
		List<String> columns = new ArrayList<String>();
		List<Field> fields = new ArrayList<>();
		Class<?> aux = type;
		while(aux!=null){	
			fields.addAll(Arrays.asList(aux.getDeclaredFields()));
			aux = (Class<?>) aux.getSuperclass();			
		}
		
		for (Field field : fields) {
			if (!"serialVersionUID".equals(field.getName())) {
				columns.add(field.getName());
			}
		}
		return columns.toArray(new String[columns.size()]);
	}

	public Object[][] getData(String[] columns) {
		List<T> input = getObjects();
		Object[][] data = new Object[input.size()][columns.length];
		for (int i = 0; i < input.size(); i++) {
			T object = input.get(i);
			for (int j = 0; j < columns.length; j++) {
				try {
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(columns[j], type);
					Method method = propertyDescriptor.getReadMethod();
					Object value = method.invoke(object);
					data[i][j] = value;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return data;
	}

	public abstract List<T> getObjects();

}
