package presentation.spendingAccount;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import model.SpendingAccount;
import presentation.DefaultPanel;
import processing.Bank;

public class DeleteSpendingAccountPanel extends DefaultPanel {

	private static final long serialVersionUID = -4375942800712256409L;

	private final JComboBox<SpendingAccount> spendingAccount;

	public DeleteSpendingAccountPanel() {
		super("Delete Spending Account");
		List<SpendingAccount> spendingAccountList = Bank.getInstance().findSpendingAccount();
		spendingAccount = new JComboBox<SpendingAccount>(new Vector<>(spendingAccountList));
		createInputFields();
	}

	public void createInputFields() {
		addInputLabel("Spending Account:", 30, 100, 150, 40);
		spendingAccount.setBounds(200, 100, 300, 30);
		add(spendingAccount);

		JButton createButton = new JButton("Remove");
		createButton.setBounds(450, 350, 100, 50);
		createButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!(spendingAccount.getSelectedItem() instanceof SpendingAccount)) {
					JOptionPane.showMessageDialog(null, "Select first Account!");
					return;
				}
				SpendingAccount account = (SpendingAccount) spendingAccount.getSelectedItem();
				if (Bank.getInstance().removeAccount(account,account.getPerson())) {
					JOptionPane.showMessageDialog(null, "Account removed!");
				} else {
					JOptionPane.showMessageDialog(null, "Unable to remove account!");
				}
			}
		});
		add(createButton);
	}
}