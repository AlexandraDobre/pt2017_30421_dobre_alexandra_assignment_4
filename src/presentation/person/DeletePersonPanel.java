package presentation.person;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import model.Person;
import presentation.DefaultPanel;
import processing.Bank;

public class DeletePersonPanel extends DefaultPanel {

	private static final long serialVersionUID = -4375942800712256409L;

	private final JComboBox<Person> persons;

	public DeletePersonPanel() {
		super("Delete Person");
		List<Person> Persons = new ArrayList<>(Bank.getInstance().findPersons());
		persons = new JComboBox<Person>(new Vector<>(Persons));
		createInputFields();
	}

	public void createInputFields() {
		addInputLabel("Person:", 30, 100, 150, 40);
		persons.setBounds(200, 100, 300, 30);
		add(persons);
		JButton createButton = new JButton("Remove");
		createButton.setBounds(450, 350, 100, 50);
		createButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!(persons.getSelectedItem() instanceof Person)) {
					JOptionPane.showMessageDialog(null, "Select first Person!");
					return;
				}
				Person person = (Person) persons.getSelectedItem();
				Bank.getInstance().removePersonById(person.getId());
				/*if (Bank.getInstance().removePersonById(person.getId())) {
					JOptionPane.showMessageDialog(null, "Person removed!");
				} else {
					JOptionPane.showMessageDialog(null, "Unable to remove Person!");
				}*/
			}
		});
		add(createButton);
	}
}
