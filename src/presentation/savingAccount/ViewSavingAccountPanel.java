package presentation.savingAccount;

import java.util.List;

import model.SavingAccount;
import presentation.ViewObjectsPanel;
import processing.Bank;

public class ViewSavingAccountPanel extends ViewObjectsPanel<SavingAccount> {

	public ViewSavingAccountPanel() {
		super("View saving account");
	}

	private static final long serialVersionUID = 1244072873629414504L;

	@Override
	public List<SavingAccount> getObjects() {
		return Bank.getInstance().findSavingAccount();
	}

}
