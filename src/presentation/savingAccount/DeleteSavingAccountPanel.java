package presentation.savingAccount;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import model.SavingAccount;
import presentation.DefaultPanel;
import processing.Bank;

public class DeleteSavingAccountPanel extends DefaultPanel {

	private static final long serialVersionUID = -4375942800712256409L;

	private final JComboBox<SavingAccount> savingAccount;

	public DeleteSavingAccountPanel() {
		super("Delete Saving Account");
		List<SavingAccount> savingAccountList = Bank.getInstance().findSavingAccount();
		savingAccount = new JComboBox<SavingAccount>(new Vector<>(savingAccountList));
		createInputFields();
	}

	public void createInputFields() {
		addInputLabel("Saving Account:", 30, 100, 150, 40);
		savingAccount.setBounds(200, 100, 300, 30);
		add(savingAccount);

		JButton createButton = new JButton("Remove");
		createButton.setBounds(450, 350, 100, 50);
		createButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!(savingAccount.getSelectedItem() instanceof SavingAccount)) {
					JOptionPane.showMessageDialog(null, "Select first Account!");
					return;
				}
				SavingAccount account = (SavingAccount) savingAccount.getSelectedItem();
				if (Bank.getInstance().removeAccount(account,account.getPerson())) {
					JOptionPane.showMessageDialog(null, "Account removed!");
				} else {
					JOptionPane.showMessageDialog(null, "Unable to remove account!");
				}
			}
		});
		add(createButton);
	}
}
