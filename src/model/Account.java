package model;

import java.io.Serializable;
import java.util.Date;
import java.util.Observable;

public abstract class Account extends Observable implements Serializable {

	private static final long serialVersionUID = 4481496628418513719L;

	private Person person;
	private Date createDate;
	private float availableSum;
	private int id;

	public Account(Person person, Date createDate, float availableSum, int id) {
		super();
		this.person = person;
		this.createDate = createDate;
		this.availableSum = availableSum;
		this.id = id;
	}

	public Account(int id) {
		this.id = id;
	}

	public Account(Person person, float availableSum, int id) {
		this.person = person;
		this.availableSum = availableSum;
		this.id = id;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public float getAvailableSum() {
		return availableSum;
	}

	public void setAvailableSum(int availableSum) {
		this.availableSum = availableSum;
		//setChanged();
		//notifyObservers(availableSum);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setAvailableSum(float availableSum) {
		this.availableSum = availableSum;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Account " + id + " Person " + person;
	}

}
