package presentation.person;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import model.Person;
import presentation.DefaultPanel;
import processing.Bank;

public class CreateNewPersonPanel extends DefaultPanel {

	private static final long serialVersionUID = 2526501659096811995L;

	private final JTextField name;
	private final JTextField address;
	private final JTextField age;
	private final JTextField id;

	public CreateNewPersonPanel() {
		super("Create person");
		name = new JTextField();
		address = new JTextField();
		age = new JTextField();
		id = new JTextField();
		createInputFields();
	}

	public void createInputFields() {

		addInputLabel("ID:", 30, 100, 150, 40);
		id.setBounds(200, 100, 300, 30);
		add(id);

		addInputLabel("Name:", 30, 150, 150, 40);
		name.setBounds(200, 150, 300, 30);
		add(name);

		addInputLabel("Address:", 30, 200, 150, 40);
		address.setBounds(200, 200, 300, 30);
		add(address);

		addInputLabel("Age:", 30, 250, 150, 40);
		age.setBounds(200, 250, 300, 30);
		add(age);

		JButton createButton = new JButton("Create person");
		createButton.setBounds(450, 300, 200, 50);
		createButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String nameValue = name.getText();
				String addressValue = address.getText();
				String ageTextValue = age.getText();
				String idTextValue = id.getText();
				Integer ageValue = null;
				Integer idValue = null;
				try {
					ageValue = Integer.valueOf(ageTextValue);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, "Age is not a number!");
					return;
				}
				try {
					idValue = Integer.valueOf(idTextValue);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, "Id is not a number!");
					return;
				}
				Person person = new Person(nameValue, idValue, ageValue, addressValue);
				try {
					Bank.getInstance().addPersons(person);
					JOptionPane.showMessageDialog(null, "Person added!");
				} catch (AssertionError ex) {
					JOptionPane.showMessageDialog(null, ex.getMessage());
				}
			}
		});
		add(createButton);
	}
}
