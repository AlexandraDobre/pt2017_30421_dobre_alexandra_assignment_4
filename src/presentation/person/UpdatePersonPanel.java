package presentation.person;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import model.Person;
import presentation.DefaultPanel;
import processing.Bank;

public class UpdatePersonPanel extends DefaultPanel {

	private static final long serialVersionUID = -4375942800712256409L;

	private final JTextField name;
	private final JTextField address;
	private final JTextField age;
	private final JComboBox<Person> persons;

	public UpdatePersonPanel() {
		super("Update person");
		name = new JTextField();
		address = new JTextField();
		age = new JTextField();
		persons = new JComboBox<Person>(new Vector<>(Bank.getInstance().findPersons()));
		createInputFields();
	}

	public void createInputFields() {

		addInputLabel("Person:", 30, 100, 150, 40);
		persons.setBounds(200, 100, 300, 30);
		add(persons);

		addInputLabel("Name:", 30, 150, 150, 40);
		name.setBounds(200, 150, 300, 30);
		add(name);

		addInputLabel("Address:", 30, 200, 150, 40);
		address.setBounds(200, 200, 300, 30);
		add(address);

		addInputLabel("Age:", 30, 250, 150, 40);
		age.setBounds(200, 250, 300, 30);
		add(age);

		JButton createButton = new JButton("Update");
		createButton.setBounds(450, 350, 100, 50);
		createButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String nameValue = name.getText();
				String addressValue = address.getText();
				String ageTextValue = age.getText();
				Integer ageValue = null;
				try {
					ageValue = Integer.valueOf(ageTextValue);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, "Age is not a number!");
					return;
				}
				if (!(persons.getSelectedItem() instanceof Person)) {
					JOptionPane.showMessageDialog(null, "Select first person!");
					return;
				}

				Integer id = ((Person) persons.getSelectedItem()).getId();
				Person person = new Person(nameValue, id, ageValue, addressValue);
				if (Bank.getInstance().updatePerson(person)) {
					JOptionPane.showMessageDialog(null, "Person updated!");
				} else {
					JOptionPane.showMessageDialog(null, "Unable to update person!");
				}
			}
		});
		add(createButton);
	}
}
