package start;

import java.util.logging.Logger;

import presentation.View;

public class Main {

	protected static final Logger LOGGER = Logger.getLogger(Main.class.getName());

	public static void main(String[] args){
		View.getInstance();
	}
}
