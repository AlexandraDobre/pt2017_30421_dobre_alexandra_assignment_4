package presentation;

public class IntroPanel extends DefaultPanel {

	private static final long serialVersionUID = -166503932009828259L;

	public IntroPanel() {
		super("About");
		initializeContent();
	}

	public void initializeContent() {
		addInputLabel("Homework 4: Bank management",  30, 100, 400, 40);
		addInputLabel("Date: 15/05/2017",  30, 150, 150, 40);
		addInputLabel("Student: Dobre Alexandra",  30, 200, 300, 40);
		addInputLabel("Group: 30421",  30, 250, 150, 40);
	}
}
