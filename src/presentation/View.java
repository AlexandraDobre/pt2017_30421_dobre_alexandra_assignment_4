package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import presentation.person.CreateNewPersonPanel;
import presentation.person.DeletePersonPanel;
import presentation.person.UpdatePersonPanel;
import presentation.person.ViewPersonsPanel;
import presentation.savingAccount.CreateSavingAccountPanel;
import presentation.savingAccount.DeleteSavingAccountPanel;
import presentation.savingAccount.UpdateSavingAccountPanel;
import presentation.savingAccount.ViewSavingAccountPanel;
import presentation.spendingAccount.CreateSpendingAccountPanel;
import presentation.spendingAccount.DeleteSpendingAccountPanel;
import presentation.spendingAccount.UpdateSpendingAccountPanel;
import presentation.spendingAccount.ViewSpendingAccountPanel;
import processing.Bank;

public class View extends JFrame {

	private static final long serialVersionUID = -2297164229508308968L;

	private static View INSTANCE = null;
	private static JPanel activePanel;

	public static View getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new View();
		}
		return INSTANCE;
	}

	private View() {
		setTitle("Bank Management");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1000, 700);
		activePanel = new IntroPanel();
		getContentPane().add(activePanel);
		createMenu();
		setVisible(true);
	}

	public void createMenu() {
		JMenuBar menu = new JMenuBar();
		createMenuFile(menu);
		createMenuPerson(menu);
		createMenuSavingAccount(menu);
		createMenuSpendingAccount(menu);
		// createMenu(menu);

		setJMenuBar(menu);
	}

	private void createMenuPerson(JMenuBar menu) {
		JMenu personMenu = new JMenu("Person");
		JMenuItem createPerson = new JMenuItem("Create new person");
		JMenuItem deletePerson = new JMenuItem("Delete person");
		JMenuItem updatePerson = new JMenuItem("Update person");
		JMenuItem viewPerson = new JMenuItem("View person");

		createPerson.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				View.updateViewToPanel(new CreateNewPersonPanel());
			}
		});

		deletePerson.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				View.updateViewToPanel(new DeletePersonPanel());
			}
		});

		updatePerson.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				View.updateViewToPanel(new UpdatePersonPanel());
			}
		});

		viewPerson.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				View.updateViewToPanel(new ViewPersonsPanel());
			}
		});

		personMenu.add(createPerson);
		personMenu.add(deletePerson);
		personMenu.add(updatePerson);
		personMenu.add(viewPerson);

		menu.add(personMenu);

	}

	public void createMenuFile(JMenuBar menu) {
		JMenu fileMenu = new JMenu("File");
		
		JMenuItem readDataItem = new JMenuItem("Read data");
		readDataItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Bank.getInstance().readDataAccounts();
				JOptionPane.showMessageDialog(null, "Data was read from file!");
			}
		});
		fileMenu.add(readDataItem);

		JMenuItem writeDataItem = new JMenuItem("Write data");
		writeDataItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Bank.getInstance().writeAccountsData();
				JOptionPane.showMessageDialog(null, "Data was written to file!");
			}
		});
		fileMenu.add(writeDataItem);

		JMenuItem exitItem = new JMenuItem("Exit");
		exitItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		fileMenu.add(exitItem);
		
		JMenuItem aboutItem = new JMenuItem("About");
		aboutItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				View.updateViewToPanel(new IntroPanel());
			}
		});

		
		
		fileMenu.add(aboutItem);
		menu.add(fileMenu);
	};

	public void createMenuSavingAccount(JMenuBar menu) {
		JMenu savingAccountMenu = new JMenu("Saving Account");
		JMenuItem createSavingAccountMenu = new JMenuItem("Create Saving Account");
		JMenuItem deleteSavingAccountMenu = new JMenuItem("Delete Saving Account");
		JMenuItem updateSavingAccountMenu = new JMenuItem("Update Saving Account");
		JMenuItem viewSavingAccountMenu = new JMenuItem("View Saving Account");

		
		deleteSavingAccountMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				View.updateViewToPanel(new DeleteSavingAccountPanel());
			}
		});

		updateSavingAccountMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				View.updateViewToPanel(new UpdateSavingAccountPanel());
			}
		});

		viewSavingAccountMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				View.updateViewToPanel(new ViewSavingAccountPanel());
			}
		});
		
		createSavingAccountMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				View.updateViewToPanel(new CreateSavingAccountPanel());
			}
		});

		
		savingAccountMenu.add(viewSavingAccountMenu);
		savingAccountMenu.add(createSavingAccountMenu);
		savingAccountMenu.add(deleteSavingAccountMenu);
		savingAccountMenu.add(updateSavingAccountMenu);
		menu.add(savingAccountMenu);
	};

	public void createMenuSpendingAccount(JMenuBar menu) {
		JMenu spendingAccountMenu = new JMenu("Spending Account");
		JMenuItem createSpendingAccountMenu = new JMenuItem("Create new Spending account");
		JMenuItem viewSpendingAccountMenu = new JMenuItem("View Spending account");
		JMenuItem deleteSpendingAccountMenu = new JMenuItem("Delete Spending Account");
		JMenuItem updateSpendingAccountMenu = new JMenuItem("Update Spending Account");

		
		deleteSpendingAccountMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				View.updateViewToPanel(new DeleteSpendingAccountPanel());
			}
		});

		updateSpendingAccountMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				View.updateViewToPanel(new UpdateSpendingAccountPanel());
			}
		});

		viewSpendingAccountMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				View.updateViewToPanel(new ViewSpendingAccountPanel());
			}
		});
		
		createSpendingAccountMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				View.updateViewToPanel(new CreateSpendingAccountPanel());
			}
		});

		
		spendingAccountMenu.add(viewSpendingAccountMenu);
		spendingAccountMenu.add(createSpendingAccountMenu);
		spendingAccountMenu.add(deleteSpendingAccountMenu);
		spendingAccountMenu.add(updateSpendingAccountMenu);
		menu.add(spendingAccountMenu);
	};
	

	public static void updateViewToPanel(JPanel panel) {
		View view = getInstance();
		view.getContentPane().remove(activePanel);
		view.getContentPane().add(panel);
		view.getContentPane().revalidate();
		view.getContentPane().repaint();
		activePanel = panel;
	}
}