package model;

import java.util.Date;

public class SavingAccount extends Account {

	private static final long serialVersionUID = -204708472611944790L;

	private static final int minSum = 10;

	public SavingAccount(Person person, Date createDate, float availableSum, int id, int minSum) {
		super(person, createDate, availableSum, id);
	}

	public SavingAccount(Person person, Date createDate, float availableSum, int id) {
		super(person, createDate, availableSum, id);
	}

	public int getMinSum() {
		return minSum;
	}
}
