package presentation.person;

import java.util.ArrayList;
import java.util.List;

import model.Person;
import presentation.ViewObjectsPanel;
import processing.Bank;

public class ViewPersonsPanel extends ViewObjectsPanel<Person> {


	public ViewPersonsPanel() {
		super("View persons");
	}

	private static final long serialVersionUID = 1244072873629414504L;

	@Override
	public List<Person> getObjects() {
		return new ArrayList<>(Bank.getInstance().findPersons());
	}

}
