package model;

import java.util.Date;

public class SpendingAccount extends Account {

	private static final long serialVersionUID = 552548938306977172L;

	private int currentInterest;

	public SpendingAccount(Person person, Date createDate, float sumValue, int id, int currentInterest) {
		super(person, createDate, sumValue, id);
		this.currentInterest = currentInterest;
	}

	public SpendingAccount(Person person, float sumValue, int id, int currentInterest) {
		super(person, sumValue, id);
		this.currentInterest = currentInterest;
	}

	public int getCurrentInterest() {
		return currentInterest;
	}

	public void setCurrentInterest(int currentInterest) {
		this.currentInterest = currentInterest;
	}

}
