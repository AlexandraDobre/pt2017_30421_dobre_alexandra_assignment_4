package model;

import java.io.Serializable;
/*import java.util.Observable;
import java.util.Observer;
*/
public class Person implements Serializable {

	private static final long serialVersionUID = 470810446245381607L;

	private String name;
	private int id;
	private int age;
	private String address;

	public Person(String name, int id, int age, String address) {
		super();
		this.name = name;
		this.id = id;
		this.age = age;
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return name + "( " + id + " )";
	}

	//@Override
	/*public void update(Observable o, Object arg) {
		if(o instanceof Account){
			Account newAccount = (Account) o;
			System.out.println("The account having the id "+ newAccount.getId() + ", having the available sum " + newAccount.getAvailableSum() + "changes!");
		}
		
	}*/

}
