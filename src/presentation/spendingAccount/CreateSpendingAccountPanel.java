package presentation.spendingAccount;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import model.Person;
import model.SpendingAccount;
import presentation.DefaultPanel;
import processing.Bank;

public class CreateSpendingAccountPanel extends DefaultPanel {

	private static final long serialVersionUID = 2526501659096811995L;

	
	private final JTextField sum;
	private final JTextField createDate;
	private final JTextField interest;
	private final JTextField id;
	private final JComboBox<Person> persons;

	public CreateSpendingAccountPanel() {
		super("Create New Spending Account");
		persons = new JComboBox<Person>(new Vector<>(Bank.getInstance().getBankEntries().keySet()));
		sum = new JTextField();
		createDate = new JTextField();
		interest = new JTextField();
		id = new JTextField();
		createInputFields();
	}

	public void createInputFields() {
		addInputLabel("ID:", 30, 100, 150, 40);
		id.setBounds(200, 100, 300, 30);
		add(id);
		
		addInputLabel("Client:", 30, 150, 150, 40);
		persons.setBounds(200, 150, 300, 30);
		add(persons);

		addInputLabel("Sum added:", 30, 200, 150, 40);
		sum.setBounds(200, 200, 300, 30);
		add(sum);

		addInputLabel("Creation Date:", 30, 250, 150, 40);
		createDate.setBounds(200, 250, 300, 30);
		add(createDate);
		
		addInputLabel("Give interest:", 30, 300, 150, 40);
		interest.setBounds(200, 300, 300, 30);
		add(interest);

		JButton createButton = new JButton("Create new spending account");
		createButton.setBounds(350, 400, 300, 50);
		createButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String sumTextValue = sum.getText();
				String createDateTextValue = createDate.getText();
				String interestTextValue = interest.getText();
				String idTextValue = id.getText();
				Float sumValue = null;
				Date date = null;
				Integer interest = null;
				Integer idValue = null;
				SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
				try {
					date = format.parse(createDateTextValue);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, "Date doesn't have the appropriate fields!");
					return;
				}
				try {
					sumValue = Float.valueOf(sumTextValue);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, "Sum is not a float number!");
					return;
				}
				try {
					interest = Integer.valueOf(interestTextValue);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, "Interest is not a float number!");
					return;
				}
				try {
					idValue = Integer.valueOf(idTextValue);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, "Id should be a number!");
					return;
				}
				if (!(persons.getSelectedItem() instanceof Person)) {
					JOptionPane.showMessageDialog(null, "Select first person!");
					return;
				}
				Person person = (Person) persons.getSelectedItem();
				SpendingAccount account = new SpendingAccount(person, date, sumValue, idValue, interest);
				try {
					Bank.getInstance().addAccount(account, person);
					JOptionPane.showMessageDialog(null, "Account added!");
				} catch (AssertionError ex) {
					JOptionPane.showMessageDialog(null, ex.getMessage());
				}
			}
		});
		add(createButton);
	}
}
