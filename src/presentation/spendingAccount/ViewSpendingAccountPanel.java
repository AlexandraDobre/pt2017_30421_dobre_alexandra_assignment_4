package presentation.spendingAccount;

import java.util.List;

import model.SpendingAccount;
import presentation.ViewObjectsPanel;
import processing.Bank;

public class ViewSpendingAccountPanel extends ViewObjectsPanel<SpendingAccount> {

	public ViewSpendingAccountPanel() {
		super("View spending account");
	}

	private static final long serialVersionUID = 1244072873629414504L;

	@Override
	public List<SpendingAccount> getObjects() {
		return Bank.getInstance().findSpendingAccount();
	}

}
